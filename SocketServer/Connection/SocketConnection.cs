﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace SocketServer
{
    public delegate void StopConnection();
    public class SocketConnection : ISocketConnection
    {
        protected TcpClient tcpClient;
        protected List<ICommandHandler> commandHandlers;
        protected StopConnection stopCallback;

        public SocketConnection(
            TcpClient tcpClient, 
            List<ICommandHandler> commandHandlers,
            StopConnection stopCallback
            )
        {
            this.tcpClient = tcpClient;
            this.commandHandlers = commandHandlers;
            this.stopCallback = stopCallback;
        }

        public async void Start()
        {
            await Task.Run(() => {
                NetworkStream stream = tcpClient.GetStream();
                this.SendMessage("Welcome\r\n", stream);
                string request = "";
                while (true)
                {
                    request = this.ReadMessage(stream);
                    if (request == null)
                        break;
                    string responce = this.RunHandlers(request);
                    this.SendMessage(responce, stream);
                }
                this.stopCallback();
            });
        }

        protected string ReadMessage(NetworkStream stream)
        {
            Byte[] bytes = new Byte[256];
            int i = 0;
            string message = "";
            while ((i = stream.Read(bytes, 0, bytes.Length)) != 0)
            {
                String data = Encoding.ASCII.GetString(bytes, 0, i);
                switch (data)
                {
                    case "\r\n":
                        return message;
                    case "\b":
                        message = message.Remove(message.Length - 1);
                        break;
                    default:
                        message += data;
                        break;
                }
            }
            return null;
        }

        protected void SendMessage(string data, NetworkStream stream)
        {
            byte[] msg = Encoding.ASCII.GetBytes(data);
            stream.Write(msg, 0, msg.Length);
        }

        protected string RunHandlers(string message)
        {
            foreach (ICommandHandler item in this.commandHandlers)
            {
                if (item.СheckСonformity(message))
                {
                    return item.Run(message);
                }
            }
            return null;
        }
    }
}
