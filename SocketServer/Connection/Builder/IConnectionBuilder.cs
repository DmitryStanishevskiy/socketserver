﻿using System.Collections.Generic;
using System.Net.Sockets;

namespace SocketServer
{
    public interface IConnectionBuilder
    {
        ISocketConnection Build(TcpClient tcpClient, List<IConnectionHeader> headers);
    }
}
