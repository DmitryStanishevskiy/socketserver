﻿using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;

namespace SocketServer
{
    public class ConnectionBuilder : IConnectionBuilder
    {
        public ISocketConnection Build(TcpClient tcpClient, List<IConnectionHeader> headers)
        {
            IConnectionHeader connectionHeader = this.CreateConnectionHeader(tcpClient, headers);
            return new SocketConnection(
                tcpClient, 
                this.BuildCommandHandlers(connectionHeader, headers),
                () => { headers.Remove(connectionHeader); });
        }

        protected List<ICommandHandler> BuildCommandHandlers(IConnectionHeader connectionHeader, List<IConnectionHeader> headers)
        {
            return new List<ICommandHandler>()
            {
                new NumberCommandHandler(connectionHeader),
                new ListCommandHandler(headers),
                new VoidCommandHandler(),
                new UnknownCommandHandler()
            };
        }

        protected string GetIP(TcpClient tcpClient)
        {
            IPEndPoint endPoint = tcpClient.Client.RemoteEndPoint as IPEndPoint;
            return $"{ endPoint.Address }:{ endPoint.Port }";
        }

        protected IConnectionHeader CreateConnectionHeader(TcpClient tcpClient, List<IConnectionHeader> headers)
        {
            IConnectionHeader connectionHeader = new ConnectionHeader(this.GetIP(tcpClient));
            headers.Add(connectionHeader);
            return connectionHeader;
        }
    }
}
