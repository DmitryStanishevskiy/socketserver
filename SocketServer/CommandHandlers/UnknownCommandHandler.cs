﻿
namespace SocketServer
{
    public class UnknownCommandHandler : ICommandHandler
    {
        public string Run(string message)
        {
            return $"Unknown command { message }\r\n";
        }

        public bool СheckСonformity(string message)
        {
            return true;
        }
    }
}
