﻿using System.Collections.Generic;

namespace SocketServer
{
    public class ListCommandHandler : ICommandHandler
    {
        protected List<IConnectionHeader> headers;
        public ListCommandHandler(List<IConnectionHeader> headers)
        {
            this.headers = headers;
        }

        public string Run(string message)
        {
            string responce = "";
            foreach(IConnectionHeader item in headers)
            {
                responce += $"IP: { item.IP }, sum = { item.Sum }\r\n";
            }
            return responce;
        }

        public bool СheckСonformity(string message)
        {
            return message.ToLower().Trim().CompareTo("list") == 0;
        }
    }
}
