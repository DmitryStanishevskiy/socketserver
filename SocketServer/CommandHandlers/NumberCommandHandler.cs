﻿using System;
using System.Text.RegularExpressions;

namespace SocketServer
{
    public class NumberCommandHandler : ICommandHandler
    {
        protected IConnectionHeader connectionHeader;

        public NumberCommandHandler(IConnectionHeader connectionHeader)
        {
            this.connectionHeader = connectionHeader;
        }

        public string Run(string message)
        {
            connectionHeader.Sum += Convert.ToInt32(message);
            return $"Total: { connectionHeader.Sum }\r\n";
        }

        public bool СheckСonformity(string message)
        {
            Regex regex = new Regex("^[0-9]+$");
            return regex.IsMatch(message);
        }
    }
}
