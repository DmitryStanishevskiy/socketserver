﻿using System;

namespace SocketServer
{
    class Program
    {
        static void Main(string[] args)
        {
            if(args.Length > 0)
            {
                IAdder adder = new Adder(Convert.ToInt32(args[0]));
                adder.Run();
            }
        }
    }
}
