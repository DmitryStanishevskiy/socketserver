﻿
namespace SocketServer
{
    public class ConnectionHeader : IConnectionHeader
    {
        public string IP { private set; get; }

        public int Sum { set; get; }

        public ConnectionHeader(string IP)
        {
            this.IP = IP;
            this.Sum = 0;
        }
    }
}
