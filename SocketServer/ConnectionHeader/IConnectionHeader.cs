﻿
namespace SocketServer
{
    public interface IConnectionHeader
    {
        string IP { get; }
        int Sum { set; get; }
    }
}
