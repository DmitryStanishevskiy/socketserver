﻿using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;

namespace SocketServer
{
    public class Adder : IAdder
    {
        protected int port;
        protected List<IConnectionHeader> headers;
        protected IConnectionBuilder connectionBuilder;
        
        public Adder(int port)
        {
            this.port = port;
            this.headers = new List<IConnectionHeader>();
            this.connectionBuilder = new ConnectionBuilder();
        }

        public void Run()
        {
            TcpListener server = null;
            {
                IPAddress localAddr = IPAddress.Parse("127.0.0.1");

                server = new TcpListener(localAddr, this.port);
                server.Start();

                while (true)
                {
                    TcpClient tcpClient = server.AcceptTcpClient();
                    ISocketConnection socketConnection = this.connectionBuilder.Build(tcpClient, headers);
                    socketConnection.Start();
                }
            }
        }


    }
}
